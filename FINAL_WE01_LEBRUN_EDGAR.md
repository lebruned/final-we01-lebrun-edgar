# Sujet 2 : Le Web est-il devenu trop compliqué ?

Ecrit par Edgar LEBRUN pour le final de WE01 (A20).

## Introduction

L'article en question, nommé *Le Web est-il devenu trop compliqué ?*, a été écrit par Stéphane Bortzmeyer sur Framablog et publié le 30 décembre 2020. L'extrait de cet article montre que le Web est trop compliqué, notamment pour le développement d'un navigateur Web. Il présente ensuite une alternative au Web, le projet Gemini, permettant de simplifier la navigation, que ce soit entre le client et le serveur, ou pour l'utilisateur lui-même. L'extrait comporte de plus un commentaire à l'article critiquant un navigateur Web léger, Dillo, ne fonctionnant pas sur certains sites, ainsi que la réponse de Stéphane Bortzmeyer à ce commentaire.

Dans cet extrait, comment Stéphane Bortzmeyer montre-t-il que le Web est devenu trop compliqué et que cela implique de nombreuses conséquences ? Tout d'abord, nous verrons en quoi le Web est "compliqué" notamment en mobilisant les connaissances sur le fonctionnement du Web et d'Internet. Nous verrons ensuite quelles sont les conséquences de la complexité du Web.

Enfin, nous nous intéresserons au projet Gemini. Le but de cette dernière partie sera de montrer comment des concepts des chaînes éditoriales pourraient être utilisés pour rendre des contenus disponibles dans le cadre du projet Gemini.

## La complexité du Web

### En quoi le Web est-il compliqué ?

Tout d'abord, nous nous intéressons à la critique de Stéphane Bortzmeyer, qui juge que le Web est trop compliqué. Dans cet extrait, il s'intéresse particulièrement au développement d'un navigateur, chose qui est visiblement devenue compliquée aujourd'hui : "Elle se traduit par des logiciels beaucoup plus complexes [...] très peu d'organisations pouvant aujourd'hui développer un navigateur Web".

#### Un nombre d'acteurs et d'intermédiaires trop important

Selon Stéphane Borztmeyer, la complexité du Web est en partie due au fait qu'il y a un très grands nombres d'acteurs nécessaires pour afficher une page Internet depuis un serveur sur le navigateur d'un utilisateur : "Et encore tout ne tourne pas sur votre machine. Derrière votre écran, l'affichage de la moindre page Web va déclencher d'innombrables opérations sur des machines que vous ne voyez pas". On peut citer des acteurs matériels (les "machines que vous ne voyez pas") et des nombreux intermédiaires : 
* Le client (l'utilisateur avec son ordinateur, son téléphone portable, etc.) qui demande à afficher un contenu sur son navigateur
* Les FAI (fournisseurs d'accès à Internet) permettant aux utilisateurs d'avoir une connexion Internet, via une $box$ par exemple
* Les routeurs (dont les $boxs$ font partie) qui vont transférer les données sur différentes routes (destinations)
* Les FCA (fournisseurs de contenu et d'applications), qui proposent le contenu
* Le serveur sur lequel est stocké le contenu
* L'hébergeur, propriétaire du serveur
* Le transitaire, qui achemine les données entre les différentes régions du monde, notamment via un réseau physique (cable, fibre optique, etc.)

On remarque donc qu'il y a un très grand nombre d'acteurs et d'intermédiaires nécessaires pour simplement afficher une page sur un navigateur Web. Cela est un premier élément montrant la complexité du Web.

#### Des protocoles complexes

Or, les données transmises entre les différents acteurs ne sont pas interprétables directement : il faut des protocoles pour pouvoir les utiliser. Là aussi, plusieurs protocoles existent : 
* Le TCP (*Transmission Control Protocol*) permettant une connexion fiable entre un client et un serveur en échangeant des segments TCP
* L'*Internet Protocol* (IP), qui permet le routage des données entre deux adresses IP
* Les noms de domaines permettant d'avoir une adresse plus "lisible" pour un humain qu'une adresse IP
* Les DNS (*Domain Name System*) permettant d'associer une adresse IP à un nom de domaine
* Le HTTP (*HyperText Transfert Protocol*) permettant au client d'obtenir des ressources depuis un serveur
* Le HTTPS, version "sécurisée" du HTTP assurant un chiffrement des données transférées
* etc.

Ainsi, pour afficher une page Web, un client va effectuer une requête (par exemple HTTP) à une URL (*Uniform Resource Locator*), associée à un nom de domaine, qu'un DNS va devoir attribuer à une adresse IP pour accéder à la ressource, etc. 

Le Web est donc quelque chose de complexe, de part la quantité d'intermédiaires nécessaires ainsi que les proptocoles.

### Les conséquences de la complexité du Web

Stéphane Bortzmeyer dénonce la complexité du Web par les conséquences de cette complexité. Si à première vue on peut penser que ce n'est pas réellement un problème, car en tant qu'utilisateur nous n'avons pas à faire à cette complexité (on ne saisit que des mots dans un moteur de recherche pour afficher des résultats), cela se dément par les nombreuses conséquences que le côté compliqué du Web a pour ses utilisateurs : "« OK, les techniques utilisées dans le Web sont compliquées mais cela ne concerne que les développeuses et développeurs, non ? » Eh bien non car cette complication a des conséquences pour tous et toutes".

#### Un marché du navigateur Web concentré

L'une des premières conséquences est le fait qu'il est compliqué de développer un navigateur Web, on a donc une concentration dans le marché du navigatteur Web, notamment par une concurrence réduite ("La concurrence a diminué sérieusement"). On peut voir cela par le fait que les navigateurs Web (il n'y en a que trois de cités : "Chrome, Edge et Safari", ce qui montre le fait qu'il y en a peu) utilisent tous un même moteur de rendu (WebKit).

#### La publicité, conséquence néfaste

La publicité est aussi pointée du doigt : 

> Derrière votre écran, l'affichage de la moindre page Web va déclencher d'innombrables opérations sur des machines que vous ne voyez pas, comme les calculs des entreprises publicitaires qui vont, en temps réel, déterminer les « meilleures » publicités à vous envoyer dans la figure ou comme l'activité de traçage des utilisateurs, notant en permanence ce qu'ils font, d'où elles viennent et de nombreuses autres informations, dont beaucoup sont envoyées automatiquement par votre navigateur Web, qui travaille au moins autant pour l'industrie publicitaire que pour vous.

Ainsi, l'une des conséquences de la complexité du Web est le fait que les nombreuses fonctions disponibles grâce au Web (notamment les *cookies* permettant aux sites de conserver des données personnelles de l'utilisateur) permettront à des entreprises publicitaires de tracer notre utilisation du Web afin de nous afficher des publicités ciblées.

#### Le Web évolue trop rapidement

Une dernière critique de la complexité du Web est que celui-ci évolue sans cesse : dans cet extrait, on peut prendre pour exemple le commentaire de Tom qui critique le fait qu'un navigateur léger ne puisse pas afficher beaucoup de pages Web.

Stéphane Bortzmeyer utilise ce commentaire pour montrer que cela révèle que le Web n'arrête pas d'évoluer, sans grand intérêt : en 2020, il est impossible d'afficher une page sur un navigateur de 2015. Or, même s'il y a 5 ans d'écart, cela n'est pas normal car les protocoles utilisés et les langages (HTML et CSS) datent de bien avant.

## Le projet Gemini

Le projet Gemini est une alternative au Web qui est plus simple et moins complexe. Cela permet de contrer les conséquences négatives mentionnées auparavant. Nous allons imaginer comment on pourrait mobiliser certains concepts des chaînes éditoriales pour rendre des contenus disponibles dans le cadre de ce projet.

### Les concept de polymorphisme

Les chaînes éditoriales permettent de prendre en compte le principe de polymorphisme et de rééditorialisation.

Le polymorphisme permet, à partir d'une source unique, de la transformer en différentes formes et supports, par exemple HTML, PDF, sous forme de présentation (diaporama), etc.

Dans le cas du projet Gemini, on peut imaginer que quelqu'un souhaite mettre à disposition un document via le projet Gemini, mais aussi de manière plus "classique" directement sur le Web. Grâce au polymorphisme d'une chaîne éditoriale, on pourrait imaginer que l'auteur du document n'aurait à l'écrire qu'un seule fois sur une chaîne éditoriale (par exemple Scenari), puis il aurait la possibilité de le transformer facilement (c'est le logiciel qui s'en chargerait) sous deux formats : un format HTML pour le Web classique, ainsi qu'un autre format correspondant au projet Gemini. Cela constituerait un gain de temps pour l'auteur (il n'aurait besoin de ne rédiger qu'une fois sur un éditeur XML), qui de plus pourrait viser un public plus large, composé à la fois des utilisateurs du Web et des utilisateurs de Gemini.

## Conclusion

Ainsi, Stéphane Bortzmeyer critique le fait que le Web est devenu trop compliqué (notamment par le nombre d'intermédiaires, les protocoles). Cette complexité a des conséquences, y compris pour les simples utilisateurs : publicité, pistage de notre utilisation d'Internet, impossibilité dans certains cas d'utiliser des navigateurs légers, etc.

Une alternative est présentée par Stéphane Bortzmeyer : le projet Gemini, qui est une alternative plus simple, avec moins d'intermédiaires, de protocoles et de fonctionnalités souvent néfastes pour l'utilisateur. Nous avons pris cet exemple pour montrer que les chaînes éditoriales pourraient être utiles dans ce cas grâce au concept de polymorphisme.

## Licence

Ce contenu est sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Elle permet : 
* de redifuser le contenu
* de l'adapter (le modifier)

Sous les conditions suivantes : 
* attribution (il faut créditer l'auteur, mentionner les changements appliqués, donner le lien de la licence)
* il faut repartager dans les mêmes conditions, c'est-à-dire avec la même licence.